export default (form, timeOut) => {
  let formId = form.getAttribute('data-form-id');
  fbq('track', 'CompleteRegistration');
  yaCounter36459695.reachGoal('order'+formId);
   dataLayer.push({'event': 'formsubmit'});
   /*
  if(formId == 1 || formId == 2)
      location.replace('https://www.youtube.com/watch?v=knLOYhr-PeE&t=2s');
  if(formId == 5)
      location.replace('/18041102_SBO%20Solution%20brief_web.pdf');
*/
   if(formId == 4)
       location.replace('http://global.sap.com/community/ebook/2015-sap-business-one-cis/index.html');
  setTimeout(() => {
    form.reset();
    $.magnificPopup.close();
  }, timeOut);
}