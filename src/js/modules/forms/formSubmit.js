import formValidate from './formValidate';
import ajaxError from './ajax/ajaxError';
import ajaxSuccess from './ajax/ajaxSuccess';

export default () => {
  const forms = document.querySelectorAll('form');
  
  for (let form of forms) {
    form.addEventListener('submit', (e) => {
      const form = e.target;
      const inputs = form.querySelectorAll('[data-validate="true"]');
      
      e.preventDefault();
      
      formValidate(inputs);
      
      if (form.querySelectorAll('.c-error').length === 0) {
        let formFields = new FormData(form);
            formFields.append('utm_source',utm_source);
            formFields.append('utm_campaign',utm_campaign);
            formFields.append('utm_term',utm_term);
            formFields.append('site',site);
        fetch('mail.php', {
          method: 'POST',
          body: formFields
        })
          .then((response) => {
            return response;
          })
          .then(() => {
            ajaxSuccess(form);
          })
          .catch((err) => {
            ajaxError(err);
          });
      }
    });
  }
}