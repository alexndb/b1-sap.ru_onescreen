export default () => {
  let elements = [
    '.c-block-type7-item-title',
    '.c-block-type9-slider-slide',
    '.c-block-type11-item-ico'
  ];
  let matchHeight = (el) => {
    el.matchHeight({
      byRow: true
    });
  };
  
  $.each(elements, function () {
    matchHeight($(this));
  })
}