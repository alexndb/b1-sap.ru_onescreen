export default () => {
  // let closeTimeout = 300;
  let popupOptions = {
    tClose: 'Закрыть (Esc)',
    fixedBgPos: true,
    fixedContentPos: true,
    removalDelay: 300,
    mainClass: 'mfp-fade',
    callbacks: {
      open: function () {
        let currentPopupSliders = $(this.content[0]).find('.slick-slider');

        if (currentPopupSliders.length !== 0) {
          $.each(currentPopupSliders, function (index, value) {
            $(value).slick('refresh');
          });
        }
      }
      // close: function () {
      //   if (document.querySelectorAll('#popup-client-video .slick-slide').length !== 0) {
      //     for (let slide of document.querySelectorAll('#popup-client-video .slick-slide:not(.slick-cloned)')) {
      //      if (slide.classList.contains('slick-current')) {
      //        console.log(slide.dataset.slickIndex)
      //      }
      //     }
      //     // console.log(document.querySelector('.slick-slide:not(.slick-cloned)'))
      //     // console.log(document.querySelector('.slick-current').dataset.slickIndex)
      //   }
      //   if ($(this.content[0]).children(0).prevObject[0].classList.contains('mfp-iframe-scaler')) {
      //     setTimeout(() => {
      //       $.magnificPopup.open(
      //         Object.assign(
      //           popupOptions,
      //           {
      //             type: 'inline',
      //             items: {
      //               src: '#popup-client-video'
      //             }
      //           }
      //         )
      //       );
      //     }, closeTimeout);
      //   }
      // }
    }
  };

  $('.js-popup').magnificPopup(popupOptions);

  $('body').on('click', '.c-header.sticky .js-popup', function () {
    $.magnificPopup.open(
      Object.assign(
        popupOptions,
        {
          items: {
            src: $(this).attr('href')
          }
        }
      )
    );
  });

  // let popupVideoLinks = document.querySelectorAll('.js-popup-video');
  //
  // for (let link of popupVideoLinks) {
  //   link.addEventListener('click', function (e) {
  //     e.preventDefault();
  //
  //     $.magnificPopup.close();
  //
  //     setTimeout(() => {
  //       $.magnificPopup.open(
  //         Object.assign(
  //           popupOptions,
  //           {
  //             type: 'iframe',
  //             items: {
  //               src: this.getAttribute('href')
  //             }
  //           }
  //         )
  //       );
  //     }, closeTimeout);
  //   });
  // }
}