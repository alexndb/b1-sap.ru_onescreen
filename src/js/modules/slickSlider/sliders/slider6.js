export default () => {
  let slider = $('.js-slider6');
  let sliderNav = $('.js-slider6-nav');
  let sliderOptions = {
    arrows: false,
    dots: false,
    fade: true,
    asNavFor: '.js-slider6-nav',
  };
  
  let sliderNavOptions = {
    arrows: false,
    dots: false,
    slidesToShow: 6,
    asNavFor: '.js-slider6',
    focusOnSelect: true,
    responsive: [
      {
        breakpoint: 900,
        settings: {
          slidesToShow: 2,
        }
      }
    ]
  };
  
  slider.slick(sliderOptions);
  sliderNav.slick(sliderNavOptions);
}