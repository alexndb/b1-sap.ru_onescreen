export default () => {
  let slider = $('.js-slider-interface');
  let sliderOptions = {
    arrows: true,
    dots: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    adaptiveHeight: true,
    appendDots: $('.slider-interface__dots'),
    responsive: [
      {
        breakpoint: 993,
        settings: {
          arrows: false
        }
      }
    ]
  };
  let currentSlideNumber = $('.slider-interface__current');
  let sliderText = $('.slider-interface__text');

  slider.on('init', function (event, slick) {
    let currentSlideText = $(slick.$slides[slick.currentSlide]).find('.slider-interface__slide').data('text');

    sliderText.text(currentSlideText);
    currentSlideNumber.text(slick.currentSlide + 1);
    $('.slider-interface__all').text(`/${slick.$slides.length}`);
  });

  slider.on('afterChange', function (event, slick, currentSlide) {
    let currentSlideText = $(slick.$slides[slick.currentSlide]).find('.slider-interface__slide').data('text');

    sliderText.stop(true, true).fadeOut();
    sliderText.html(currentSlideText);
    sliderText.stop(true, true).fadeIn();
    currentSlideNumber.text(currentSlide + 1);
  });

  slider.slick(sliderOptions);
}