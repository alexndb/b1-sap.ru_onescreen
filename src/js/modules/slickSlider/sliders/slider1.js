export default () => {
  let slider = $('.js-slider1');
  let sliderNav = $('.js-slider1-nav');
  let sliderOptions = {
    arrows: false,
    dots: false,
    fade: true,
    asNavFor: '.js-slider1-nav',
  };

  let sliderNavOptions = {
    arrows: false,
    dots: false,
    slidesToShow: 3,
    asNavFor: '.js-slider1',
    focusOnSelect: true,
    vertical: true,
    responsive: [
      {
        breakpoint: 769,
        settings: {
          vertical: false
        }
      }
    ]
  };

  let changeTitle = (slick) => {
    let title = $(slick.$slides[slick.currentSlide]).parents('.c-popup-default').find('.c-main-title');
    let currentSlideText = $(slick.$slides[slick.currentSlide]).find('.c-block-type3-slider-nav-slide-title').text();

    title.stop(true, true).fadeOut().text(currentSlideText).stop(true, true).fadeIn();
  };

  sliderNav.on('init', function (event, slick) {
    changeTitle(slick);
  });

  sliderNav.on('afterChange', function (event, slick, currentSlide) {
    changeTitle(slick);
  });

  slider.slick(sliderOptions);
  sliderNav.slick(sliderNavOptions);
}