export default () => {
  let slider = $('.js-slider2');
  let sliderNav = $('.js-slider2-nav');
  let sliderOptions = {
    arrows: false,
    dots: false,
    fade: true,
    asNavFor: '.js-slider2-nav',
    responsive: [
      {
        breakpoint: 1,
        settings: {
          slidesToShow: 2,
        }
      },
      {
        breakpoint: 1,
        settings: {
          slidesToShow: 1,
        }
      }
    ]
  };
  
  let sliderNavOptions = {
    arrows: false,
    dots: false,
    slidesToShow: 8,
    asNavFor: '.js-slider2',
    focusOnSelect: true,
    responsive: [
      {
        breakpoint: 1200,
        settings: {
          slidesToShow: 6,
        }
      },
      {
        breakpoint: 900,
        settings: {
          slidesToShow: 4,
        }
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 3,
        }
      },
      {
        breakpoint: 500,
        settings: {
          slidesToShow: 2,
        }
      }
    ]
  };
  
  slider.slick(sliderOptions);
  sliderNav.slick(sliderNavOptions);
}