export default () => {
  let slider = $('.js-slider4');
  let sliderOptions = {
    arrows: true,
    dots: true,
    adaptiveHeight: true
  };
  
  slider.slick(sliderOptions);
}