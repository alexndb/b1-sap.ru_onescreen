import 'babel-polyfill';
import dragOff from './modules/dragOff';
import formSubmit from './modules/forms/formSubmit';
import magnificPopup from './modules/magnificPopup';
import stickyHeader from './modules/stickyHeader';
import agreement from './modules/agreement';
import inputs from './modules/inputs';
import slider1 from './modules/slickSlider/sliders/slider1';
import slider4 from './modules/slickSlider/sliders/slider4';
import sliderInterface from './modules/slickSlider/sliders/slider-interface';

dragOff();
formSubmit();
agreement();
inputs();

$(document).ready(() => {
  magnificPopup();
  stickyHeader();
  slider1();
  slider4();
  sliderInterface();
});