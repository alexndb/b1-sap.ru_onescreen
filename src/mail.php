<?php
session_start();
$get = $_POST;//$_SESSION['get'];
$site = $_SESSION['site'];

$frm_name  = "order@b1-sap.ru";
$recepient = "b1LP.by@sap.com, uliana.krepchuk@sap.com, alena.sysoi@sap.com, s.gorlov@ru.digital, la@ru.digital, ksenia.titarenko@sap.com";
$sitename  = "SAP B1 one page";
$subject   = "Новая заявка с сайта \"$sitename\"";

//$recepient = "roman@ru-lab.ru";

$name = trim($_POST["name"]);
$phone = trim($_POST["phone"]);
$email = trim($_POST["email"]);
$message = trim($_POST["text"]);
$company = trim($_POST["company"]);
$theme = trim($_POST["form_name"]);
$city = trim($_POST["city"]);
$employees = trim($_POST["employees"]);
$agreement = $_POST["agreement"];

$msg = "
Имя: <strong>$name </strong><br>
Телефон: <strong>$phone </strong><br>
E-mail: <strong>$email </strong><br>";
if($message)
    $msg .= "Сообщение: <strong>$message </strong><br>";
if($company)
    $msg .= "Компания: <strong>$company</strong><br>";
if($city)
    $msg .= "Город: <strong>$city</strong><br>";
if($employees)
    $msg .= "Кол-во сотрудников: <strong>$employees</strong><br>";
if($agreement)
    $msg .= "Согласие на обработку данных: <strong>$agreement</strong><br>";

$msg .= "Форма: <strong>$theme </strong><br> ";
$msg .="

UTM Данные:<br>     
Источник рекламы: <strong>".$get['utm_source']."  </strong><br>
Тип рекламы: <strong>".$get['utm_medium']."  </strong><br>
Содержание: <strong>".$get['utm_content']."  </strong><br>
Ключевая фраза: <strong>".$get['utm_term']."  </strong><br>
Кампания: <strong>".$get['utm_campaign']." </strong><br>

";


$config['smtp_username'] = 'sap-c4c@yandex.ru';  //Смените на адрес своего почтового ящика.
$config['smtp_port'] = '465'; // Порт работы.
$config['smtp_host'] =  'ssl://smtp.yandex.ru';  //сервер для отправки почты
$config['smtp_password'] = '0192837465';  //Измените пароль
$config['smtp_debug'] = true;  //Если Вы хотите видеть сообщения ошибок, укажите true вместо false
$config['smtp_charset'] = 'utf-8';	//кодировка сообщений. (windows-1251 или utf-8, итд)
$config['smtp_from'] = 'B1 SAP'; //Ваше имя - или имя Вашего сайта. Будет показывать при прочтении в поле "От кого"

function smtpmail($to='', $mail_to, $subject, $message, $headers='') {
    global $config;
    $SEND =	"Date: ".date("D, d M Y H:i:s") . " UT\r\n";
    $SEND .= 'Subject: =?'.$config['smtp_charset'].'?B?'.base64_encode($subject)."=?=\r\n";
    if ($headers) $SEND .= $headers."\r\n\r\n";
    else
    {
        $SEND .= "Reply-To: ".$config['smtp_username']."\r\n";
        $SEND .= "To: \"=?".$config['smtp_charset']."?B?".base64_encode($to)."=?=\" <$mail_to>\r\n";
        $SEND .= "MIME-Version: 1.0\r\n";
        $SEND .= "Content-Type: text/html; charset=\"".$config['smtp_charset']."\"\r\n";
        $SEND .= "Content-Transfer-Encoding: 8bit\r\n";
        $SEND .= "From: \"=?".$config['smtp_charset']."?B?".base64_encode($config['smtp_from'])."=?=\" <".$config['smtp_username'].">\r\n";
        $SEND .= "X-Priority: 3\r\n\r\n";
    }
    $SEND .=  $message."\r\n";
    if( !$socket = fsockopen($config['smtp_host'], $config['smtp_port'], $errno, $errstr, 30) ) {
        if ($config['smtp_debug']) echo $errno."<br>".$errstr;
        return false;
    }

    if (!server_parse($socket, "220", __LINE__)) return false;

    fputs($socket, "HELO " . $config['smtp_host'] . "\r\n");
    if (!server_parse($socket, "250", __LINE__)) {
        if ($config['smtp_debug']) echo '<p>Не могу отправить HELO!</p>';
        fclose($socket);
        return false;
    }
    fputs($socket, "AUTH LOGIN\r\n");
    if (!server_parse($socket, "334", __LINE__)) {
        if ($config['smtp_debug']) echo '<p>Не могу найти ответ на запрос авторизаци.</p>';
        fclose($socket);
        return false;
    }
    fputs($socket, base64_encode($config['smtp_username']) . "\r\n");
    if (!server_parse($socket, "334", __LINE__)) {
        if ($config['smtp_debug']) echo '<p>Логин авторизации не был принят сервером!</p>';
        fclose($socket);
        return false;
    }
    fputs($socket, base64_encode($config['smtp_password']) . "\r\n");
    if (!server_parse($socket, "235", __LINE__)) {
        if ($config['smtp_debug']) echo '<p>Пароль не был принят сервером как верный! Ошибка авторизации!</p>';
        fclose($socket);
        return false;
    }
    fputs($socket, "MAIL FROM: <".$config['smtp_username'].">\r\n");
    if (!server_parse($socket, "250", __LINE__)) {
        if ($config['smtp_debug']) echo '<p>Не могу отправить комманду MAIL FROM: </p>';
        fclose($socket);
        return false;
    }
    $mails = explode(",",$mail_to);
    foreach($mails as $m)
    {
        $m = trim($m);
        fputs($socket, "RCPT TO: <" . $m . ">\r\n");

        if (!server_parse($socket, "250", __LINE__)) {
            if ($config['smtp_debug']) echo '<p>Не могу отправить комманду RCPT TO: </p>';
            fclose($socket);
            return false;
        }
    }
    fputs($socket, "DATA\r\n");

    if (!server_parse($socket, "354", __LINE__)) {
        if ($config['smtp_debug']) echo '<p>Не могу отправить комманду DATA</p>';
        fclose($socket);
        return false;
    }
    fputs($socket, $SEND."\r\n.\r\n");

    if (!server_parse($socket, "250", __LINE__)) {
        if ($config['smtp_debug']) echo '<p>Не смог отправить тело письма. Письмо не было отправленно!</p>';
        fclose($socket);
        return false;
    }
    fputs($socket, "QUIT\r\n");
    fclose($socket);
    return TRUE;
}

function server_parse($socket, $response, $line = __LINE__) {
    global $config;
    while (@substr($server_response, 3, 1) != ' ') {
        if (!($server_response = fgets($socket, 256))) {
            if ($config['smtp_debug']) echo "<p>Проблемы с отправкой почты!</p>$response<br>$line<br>";
            return false;
        }
    }
    if (!(substr($server_response, 0, 3) == $response)) {
        if ($config['smtp_debug']) echo "<p>Проблемы с отправкой почты!</p>$response<br>$line<br>";
        return false;
    }
    return true;
}


smtpmail($recepient, $recepient, $subject, $msg);

//mail($recepient, $subject, $msg, "From: $frm_name" . "\r\n" . "Reply-To: $frm_name" . "\r\n" . "X-Mailer: PHP/" . phpversion() . "\r\n" . "Content-type: text/html; charset=\"utf-8\"");
